import requests

'''Translate "Requesto('http://www.foo.com').bar['42'].baz(bazoo=0)" into an
http request to http://www.foo.com/bar/42/baz?bazoo=0.
'''

class Requesto(object):
    def __init__(self, base_url, headers={}):
        self.base_url = base_url
        self.default_headers = headers
        self.__path = []

    def __getattr__(self, name):
        self.__path.append(name)
        return self

    def __getitem__(self, name):
        self.__path.append(name)
        return self

    def __call__(self, headers=None, **kwargs):
        return self.get(headers, **kwargs)

    def get(self, headers=None, **kwargs):
        return self.request('GET', headers, **kwargs)

    def post(self, headers=None, **kwargs):
        return self.request('POST', headers, **kwargs)

    def put(self, headers=None, **kwargs):
        return self.request('PUT', headers, **kwargs)

    def delete(self, headers=None, **kwargs):
        return self.request('DELETE', headers, **kwargs)

    def request(self, method, headers=None, **kwargs):
        return self._decode_response(self._request(method, headers, **kwargs))

    def _request(self, method, headers=None, **kwargs):
        try:
            if headers is None:
                headers = self.default_headers
            url = '/'.join([self.base_url]+self.__path)
            params = self._encode_arguments(method, kwargs)
            if method == 'GET':
                response = requests.get(url, headers=headers, params=params)
            elif method == 'POST':
                response = requests.post(url, headers=headers, data=params)
            elif method == 'PUT':
                response = requests.put(url, headers=headers, data=params)
            elif method == 'DELETE':
                response = requests.delete(url, headers=headers, **params)
            else:
                raise RequestoError('Unsupported method "%s"' % method)
        finally:
            del self.__path[:]
        if response.status_code != 200:
            response.raise_for_status()
        return response

    def _encode_arguments(self, method, kwargs):
        for name, value in kwargs.items():
            kwargs[name] = self._encode_argument(method, kwargs, name, value)
        return kwargs

    def _encode_argument(self, method, kwargs, name, value):
        return str(value)

    def _decode_reponse(self, response):
        return response.json()


class RequestoError(Exception):
    pass
